import React, { Fragment } from 'react'
import { Routes, Route } from 'react-router-dom'
import Login from '../components/Login/Login'
import HomePage from '../pages/HomePage'

const AppRoute = () => {
    return (
        <Fragment>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/login" element={<Login />} />
            </Routes>
        </Fragment>
    )
}

export default AppRoute
