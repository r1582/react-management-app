import React, { Fragment } from 'react';
import { Container, Row, Col, Navbar } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import "../../assets/css/style.css";
import "../../assets/css/media.css";

const Header = () => {
    return (
        <Fragment>
            <Navbar>
                <Container>
                    <Navbar.Brand href="/">Management</Navbar.Brand>
                        <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>User Martin <a href="/login">Logout</a></Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </Fragment>
    )
}

export default Header
